﻿using System;
using System.Collections.Generic;

namespace LMS_Task.DBModels;

public partial class DemoMaster
{
    public int Id { get; set; }

    public int FolderId { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? Description { get; set; }

    public string? Attachment { get; set; }

    public bool? IsDelete { get; set; }
}
