﻿using System;
using System.Collections.Generic;

namespace LMS_Task.DBModels;

public partial class Folder
{
    public int FolderId { get; set; }

    public string? Fpath { get; set; }

    public bool IsDelete { get; set; }
}
