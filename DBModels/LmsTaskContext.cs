﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace LMS_Task.DBModels;

public partial class LmsTaskContext : DbContext
{
    public LmsTaskContext()
    {
    }

    public LmsTaskContext(DbContextOptions<LmsTaskContext> options)
        : base(options)
    {
    }

    public virtual DbSet<DemoMaster> DemoMasters { get; set; }

    public virtual DbSet<Folder> Folders { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=MR-PRIME09\\PRIME;Database=LmsTask;Integrated Security = true ;TrustServerCertificate = True ;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DemoMaster>(entity =>
        {
            entity.ToTable("DemoMaster");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("id");
            entity.Property(e => e.FolderId).ValueGeneratedOnAdd();
        });

        modelBuilder.Entity<Folder>(entity =>
        {
            entity.ToTable("Folder");

            entity.Property(e => e.FolderId).ValueGeneratedNever();
            entity.Property(e => e.Fpath)
                .HasMaxLength(10)
                .IsFixedLength()
                .HasColumnName("FPath");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
