﻿using LMS_Task.DBModels;
using LMS_Task.Services.IServices;
using Microsoft.AspNetCore.Mvc;

namespace LMS_Task.Controllers
{
    public class Demotask : Controller
    {
        private LmsTaskContext _dbContext;
        private IFolderDatas _folderDatas;
        private IFolderDatas _IfolderDatas;
        public Demotask(LmsTaskContext context, IFolderDatas folderDatas, IFolderDatas ifolderDatas)
        {
            _dbContext = context;
            _folderDatas = folderDatas;
            _IfolderDatas = ifolderDatas;
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
